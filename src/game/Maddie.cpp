#include "Maddie.h"

#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"

#include <cmath>

Maddie::Maddie()
    :x(0),
    y(0),
    z(0),
    b(0.0f),
    c(0.0f),
    name("")
{

}

Maddie::Maddie(int inX, int inY, int inZ, float inB, float inC, std::string inName)
    :x(inX),
    y(inY),
    z(inZ),
    b(inB),
    c(inC),
    name(inName)
{

}

Maddie::~Maddie()
{

}

void Maddie::setX(int x)
{
    this->x = x;
}

int Maddie::getX()
{
    return this->x;
}

void Maddie::setY(int y)
{
    this->y = y;
}

int Maddie::getY()
{
    return this->y;
}

void Maddie::setZ(int z)
{
    this->z = z;
}

int Maddie::getZ()
{
    return this->z;
}

void Maddie::setB(float b)
{
    this->b = b;
}

float Maddie::getB(void)
{
    return this->b;
}

void Maddie::setC(float c)
{
    this->c = c;
}

float Maddie::getC(void)
{
    return this->c;
}

void Maddie::setName(std::string name)
{
    this->name = name;
}

std::string Maddie::getName()
{
    return this->name;
}

bool Maddie::operator==(Maddie& other)
{
    if (x != other.x) return false;
    if (std::abs(b - other.b) > 0.00001f) return false;
    if (!(name == other.name)) return false;

    return true;
}

void Maddie::Read(InputMemoryStream& in)
{
    int len = 0;
    char temp = 0;

    in.Read(x);
    in.Read(b);
    in.Read(len);

    name.clear();

    for (int i = 0; i < len; i++)
    {
        in.Read(temp);
        name += temp;
    }

}

void Maddie::Write(OutputMemoryStream& out)
{
    out.Write(x);
    out.Write(b);

    int len = name.length();
    out.Write(len);

    for (char& c : name) //if this throws an error your compiler can't handle C++11!
        out.Write(c);

}
