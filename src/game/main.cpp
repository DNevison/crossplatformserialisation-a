
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"

#include "Bob.h"

#include <memory>
using std::shared_ptr;

#include <iostream>


#if _WIN32
#include <Windows.h>
#endif


#if _WIN32
int main(int argc, const char** argv)
{

	shared_ptr<OutputMemoryStream> out(new OutputMemoryStream());
	shared_ptr<InputMemoryStream> in;

	Bob bob;

	bob.Write(*out);

	// Setup a buffer (out is your OutputMemoryStream)
	int copyLen = out->GetLength();
	char* copyBuff = new char[copyLen];

	// Copy over the buffer
	memcpy(copyBuff, out->GetBufferPtr(), copyLen);
	// Create a new memory stream
	in.reset(new InputMemoryStream(copyBuff, copyLen));

	Bob bob1;

	bob1.Read(*in);

	if (bob == bob1)
		std::cout << "They are equal" << std::endl;
}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
