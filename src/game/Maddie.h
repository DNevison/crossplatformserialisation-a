#ifndef _MADDIE_H_
#define _MADDIE_H_

#include <string>
class InputMemoryStream;
class OutputMemoryStream;

class Maddie
{
private:
	int x, y, z;
	float b, c;

	std::string name;
public:
	Maddie();
	Maddie(int inX, int inY, int inZ, float inB, float inC, std::string inName);
	~Maddie();

	void setX(int x);
	int getX();

	void setY(int y);
	int getY();

	void setZ(int z);
	int getZ();

	void setB(float b);
	float getB(void);

	void setC(float c);
	float getC(void);

	void setName(std::string name);
	std::string getName();

	bool operator==(Maddie& other);

	void Read(InputMemoryStream& in);
	void Write(OutputMemoryStream& out);

};
#endif //_MADDIE_H_

